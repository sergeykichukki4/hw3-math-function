"use strict";

let firstNum;
let secondNum;
let mathOp;

function mathOpCheckCorrectness () {
    let op;
    do {
        op = prompt(`Enter math operation "+", "-" , "*" , "/"`);
    } while (!(op === "+" || op === "-" || op === "*" || op === "/"));
    return op;
}

function numCheckCorrectness () {
    let num;
    do {
        num = prompt(`Enter number`);
    } while (!num || isNaN(num));

    return +num;
}

function mathFunction (num1, num2, op) {
    if (op === "+") {
        return +num1 + +num2;
    }
    if (op === "-") {
        return num1 - num2;
    }
    if (op === "/") {
        return num1 / num2;
    }
    if (op === "*") {
        return num1 * num2;
    }
}

firstNum = numCheckCorrectness();
secondNum = numCheckCorrectness();

mathOp = mathOpCheckCorrectness();

console.log(mathFunction(firstNum, secondNum, mathOp));